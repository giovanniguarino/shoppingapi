using System;
using Xunit;
using Catalog.Controllers;
using Catalog.DataStore;
using Catalog.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

namespace UnitTests.Catalog.Controllers
{
    public class CatalogControllerTest
    {
        private CatalogDataStore _dataStore;
        private CatalogController _controller;

        public CatalogControllerTest()
        {
            _dataStore = new CatalogDataStore();
            _controller = new CatalogController(_dataStore);
        }

        //GET ALL ITEMS
        [Fact]
        public async Task Get_all_catalog_items_success()
        {
            var result = await _controller.GetAllCatalogItems();
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var items = (List<CatalogItem>)objectResult.Value;
            Assert.Equal(items, _dataStore.GetAllCatalogItems());
        }

        //ITEM NOT EXISTS
        [Fact]
        public async Task Get_catalog_item_not_found()
        {
            var result = await _controller.GetCatalogItem(0);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
        }

        //ITEM EXISTS
        [Fact]
        public async Task Get_catalog_item_success()
        {
            var catalogItem = _dataStore.GetAllCatalogItems().First();
            var result = await _controller.GetCatalogItem(catalogItem.Id);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var item = (CatalogItem)objectResult.Value;
            Assert.Equal(item, catalogItem);
        }


        //STOCK QTY TEST 0 OR NEGATIVE NUMBER
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task Sub_stock_item_zero_or_negative_qty_error(int qty)
        {
            var catalogItem = _dataStore.GetAllCatalogItems().First();
            var result = await _controller.SubStockQty(catalogItem.Id, qty);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
        }

        //STOCK ITEM NOT FOUND
        [Fact]
        public async Task Sub_stock_item_not_found()
        {
            var result = await _controller.SubStockQty(0, 1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
        }

        //STOCK QTY NOT AVAILABLE
        [Fact]
        public async Task Sub_stock_item_no_available_qty_error()
        {
            var catalogItem = _dataStore.GetAllCatalogItems().First();
            var result = await _controller.SubStockQty(catalogItem.Id, catalogItem.StockQty + 1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
        }

        //STOCK QTY UPDATED SUCCESS
        [Fact]
        public async Task Sub_stock_item_success()
        {
            var catalogItem = _dataStore.GetAllCatalogItems().First();
            var result = await _controller.SubStockQty(catalogItem.Id, catalogItem.StockQty);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            Assert.Equal(0, catalogItem.StockQty);
        }


    }
}
