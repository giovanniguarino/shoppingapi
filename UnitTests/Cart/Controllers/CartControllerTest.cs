using System;
using Xunit;
using Cart.Controllers;
using Cart.DataStore;
using Cart.Services;
using Cart.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;
using Moq;

namespace UnitTests.Cart.Controllers
{
    public class CartControllerTest
    {
        private ICartDataStore _dataStore;
        private Mock<ICartService> _cartService;

        public CartControllerTest()
        {
            _dataStore = new CartDataStore();
            _cartService = new Mock<ICartService>();

        }

        //CREATE CART
        [Fact]
        public async Task Create_cart_success()
        {
            var controller = new CartController(_dataStore, _cartService.Object);
            var userId = 1;
            var result = await controller.CreateCart(userId);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var cart = objectResult.Value as CartModel;
            Assert.NotNull(cart);
        }





        //CHECKOUT CART NOT FOUND
        [Fact]
        public async Task Checkout_cart_not_found()
        {
            var controller = new CartController(_dataStore, _cartService.Object);
            var cart = new CartModel{Id=0, UserId=0};
            cart.CartItems = new List<CartItem>();
            cart.CartItems.Add(new CartItem { Id = 1, Name = "MacBook", Price = 200, Qty = 1 });
            cart.Total = 200;
            var result = await controller.Checkout(cart.Id);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);

        }

        //CHECKOUT CART STOCK NOT AVAILABLE
        [Fact]
        public async Task Checkout_cart_stock_not_available_error()
        {
            _dataStore.CreateCart(1);

            var cart = _dataStore.GetAllCart().First();
            cart.CartItems.Add(new CartItem{Id=1, Name="MacBook", Price=200, Qty=2});
            cart.Total = 400;

            _cartService.Setup((c) => c.StockAvailableCart(cart)).ReturnsAsync(false);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.Checkout(cart.Id);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.Equal("Stock Qty not available!", objectResult.Value);

        }

        //CHECKOUT CART SERVICE ERROR
        [Fact]
        public async Task Checkout_cart_service_error()
        {
            _dataStore.CreateCart(1);

            var cart = _dataStore.GetAllCart().First();
            cart.CartItems.Add(new CartItem { Id = 1, Name = "MacBook", Price = 200, Qty = 1 });
            cart.Total = 200;

            var order = new OrderModel { Id = 1, UserId = cart.UserId, OrderItems = cart.CartItems, Total = cart.Total };
            _cartService.Setup((c) => c.StockAvailableCart(cart)).ReturnsAsync(true);
            _cartService.Setup((c) => c.CheckOut(cart)).ReturnsAsync(false);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.Checkout(cart.Id);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.True(cart.CartItems.Count > 0);
            Assert.Equal("Checkout error!", objectResult.Value);

        }

        //CHECKOUT CART SUCCESS
        [Fact]
        public async Task Checkout_cart_success()
        {
            _dataStore.CreateCart(1);

            var cart = _dataStore.GetAllCart().First();
            cart.CartItems.Add(new CartItem { Id = 1, Name = "MacBook", Price = 200, Qty = 1 });
            cart.Total = 200;

            var order = new OrderModel { Id = 1, UserId = cart.UserId, OrderItems = cart.CartItems, Total = cart.Total };
            _cartService.Setup((c) => c.CheckOut(cart)).ReturnsAsync(true);
            _cartService.Setup((c) => c.StockAvailableCart(cart)).ReturnsAsync(true);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.Checkout(cart.Id);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            Assert.True(cart.CartItems.Count==0);
            Assert.Equal("Checkout completed!", objectResult.Value);

        }

        //CART NOT EXISTS
        [Fact]
        public async Task Get_cart_not_found()
        {
            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.GetCart(1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
        }

        //CART EXISTS
        [Fact]
        public async Task Get_cart_success()
        {
            _dataStore.CreateCart(1);
            var controller = new CartController(_dataStore, _cartService.Object);
            var cart = _dataStore.GetAllCart().First();
            var result = await controller.GetCart(cart.Id);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var cartResult = objectResult.Value as CartModel;
            Assert.NotNull(cartResult);
        }


        //ADD CARTITEMS CART NOT EXISTS
        [Fact]
        public async Task Add_cartitems_cart_not_found()
        {
            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.AddCartItem(1, 2, 3);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
            Assert.Equal($"Cart 1 not found!", objectResult.Value);
        }

        //ADD CARTITEMS NOT EXISTS
        [Fact]
        public async Task Add_cartitems_not_found()
        {
            _dataStore.CreateCart(1);
            _cartService.Setup((c) => c.GetCatalogItem(2)).ReturnsAsync((CatalogItem)null);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.AddCartItem(1, 2, 3);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
            Assert.Equal($"Item 2 not found!", objectResult.Value);
        }

        //ADD CARTITEMS STOCK NOT AVAILABLE
        [Fact]
        public async Task Add_cartitems_stock_not_available_error()
        {
            _dataStore.CreateCart(1);

            var catalogItem = new CatalogItem { Id = 2, Name = "Ipad", Price = 100, StockQty = 2};
            _cartService.Setup((c) => c.GetCatalogItem(2)).ReturnsAsync(catalogItem);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.AddCartItem(1, 2, 3);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.Equal("Stock qty not available!", objectResult.Value);
        }

        //ADD CARTITEMS SUCCESS
        [Fact]
        public async Task Add_cartitems_success()
        {
            _dataStore.CreateCart(1);

            var catalogItem = new CatalogItem { Id = 2, Name = "Ipad", Price = 100, StockQty = 3 };
            _cartService.Setup((c) => c.GetCatalogItem(2)).ReturnsAsync(catalogItem);

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.AddCartItem(1, 2, 3);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var cartResult = objectResult.Value as CartModel;
            Assert.NotNull(cartResult);
        }

        //DELETE CARTITEMS CART NOT FOUND
        [Fact]
        public async Task Delete_cartitems_cart_not_found()
        {
            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.DeleteCartItem(1, 1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
            Assert.Equal($"Cart 1 not found!", objectResult.Value);
        }


        //ADD CARTITEMS SUCCESS
        [Fact]
        public async Task Delete_cartitems_success()
        {
            _dataStore.CreateCart(1);
            var cart = _dataStore.GetAllCart().First();
            var cartItem = new CartItem { Id = 2, Name = "Ipad", Price = 100, Qty = 3 };
            cart.CartItems.Add(cartItem);
            cart.Total = 300;

            var controller = new CartController(_dataStore, _cartService.Object);
            var result = await controller.DeleteCartItem(1, 2);
            var objectResult = result as ObjectResult;

            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var cartResult = objectResult.Value as CartModel;
            Assert.NotNull(cartResult);
            Assert.True(cartResult.CartItems.Count==0);
            Assert.True(cartResult.Total== 0);
        }

    }
}
