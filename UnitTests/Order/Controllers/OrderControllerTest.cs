﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Order.Controllers;
using Order.DataStore;
using Order.Models;
using Xunit;

namespace UnitTests.Order.Controllers
{
    public class OrderControllerTest
    {
        protected OrderController _controller;
        protected IOrderDataStore _dataStore;

        public OrderControllerTest()
        {
            _dataStore = new OrderDataStore();
            _controller = new OrderController(_dataStore);
        }

        //GET ALL ORDERS BY USERID NOT FOUND
        [Fact]
        public async Task Get_all_orders_by_userId_not_found()
        {
            var result = await _controller.GetAllOrdersByUserId(1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.NotFound, objectResult.StatusCode);
        }

        //GET ALL ORDERS BY USERID SUCCESS
        [Fact]
        public async Task Get_all_orders_by_userId_success()
        {
            var orderItems = new List<OrderItem>();
            orderItems.Add(new OrderItem{Id = 1, Name = "MacBook", Price = 200, Qty = 1});
            var order = new OrderModel{OrderItems = orderItems, Total = 200, UserId = 1};
            _dataStore.CreateOrder(order);
            var result = await _controller.GetAllOrdersByUserId(1);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            var orders = objectResult.Value as List<OrderModel>;
            Assert.NotNull(orders);
            Assert.False(orders.Count==0);
        }

        //CREATE ORDER ITEMS LIST NULL ERROR
        [Fact]
        public async Task Create_order_items_null_error()
        {
            var order = new OrderModel {Total = 200, UserId = 1 };
            var result = await _controller.Create(order);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.Equal("OrderItems can't be null!", objectResult.Value);
        }

        //CREATE ORDER ITEMS LIST EMPTY ERROR
        [Fact]
        public async Task Create_order_items_empty_error()
        {
            var orderItems = new List<OrderItem>();
            var order = new OrderModel { OrderItems = orderItems, Total = 200, UserId = 1 };
            var result = await _controller.Create(order);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.Equal("OrderItems can't be empty!", objectResult.Value);
        }

        //CREATE ORDER SUCCESS
        [Fact]
        public async Task Create_order_success()
        {
            var orderItems = new List<OrderItem>();
            orderItems.Add(new OrderItem{Id = 1, Name = "MacBook", Price = 200, Qty = 2});
            var order = new OrderModel { OrderItems = orderItems, Total = 200, UserId = 1 };
            var result = await _controller.Create(order);
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.Equal((int)HttpStatusCode.OK, objectResult.StatusCode);
            Assert.Equal("Order created!", objectResult.Value);
        }
    }
}
