﻿using System;
using System.Collections.Generic;
using Catalog.Models;
using System.Linq;

namespace Catalog.DataStore
{
    public class CatalogDataStore : ICatalogDataStore
    {
        private IDictionary<int, CatalogItem> _catalogItems;

        public CatalogDataStore()
        {
            _catalogItems = new Dictionary<int,CatalogItem>();
            CatalogItem macbook = new CatalogItem { Id = 1, Name = "MacBook", Price = 200, StockQty = 10 };
            CatalogItem ipad = new CatalogItem{ Id = 2, Name = "iPad", Price = 100, StockQty = 5 };
            _catalogItems.Add(macbook.Id, macbook);
            _catalogItems.Add(ipad.Id, ipad);
        }

        public IEnumerable<CatalogItem> GetAllCatalogItems() => _catalogItems.Values.ToList();

        public CatalogItem GetCatalogItemById(int id)
        {
            if (!_catalogItems.ContainsKey(id))
            {
                return null;
            }
            
            return _catalogItems[id];
        }

        public bool UpdateCatalogItem(CatalogItem catalogItem)
        {
            if (!_catalogItems.ContainsKey(catalogItem.Id))
            {
                return false;
            }
            _catalogItems[catalogItem.Id] = catalogItem;
            return true;
        }

    }
}
