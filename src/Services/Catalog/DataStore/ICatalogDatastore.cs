﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Catalog.Models;


namespace Catalog.DataStore
{
    public interface ICatalogDataStore
    {
        IEnumerable<CatalogItem> GetAllCatalogItems();
        CatalogItem GetCatalogItemById(int id);
    }
}