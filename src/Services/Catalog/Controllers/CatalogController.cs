﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Catalog.DataStore;
using Catalog.Models;
using System.Net;

namespace Catalog.Controllers
{
    [Route("api/[controller]")]
    public class CatalogController : Controller
    {
        protected ICatalogDataStore _dataStore;
        public CatalogController(ICatalogDataStore dataStore) =>
        _dataStore = dataStore;



        // GET ALL ITEMS FROM CATALOG 
        // GET api/catalog
        [HttpGet]
        [ProducesResponseType(typeof(List<CatalogItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult>  GetAllCatalogItems()
        {
            await Task.Delay(1000);
            return Ok(_dataStore.GetAllCatalogItems());
        }

        // GET AN ITEM FROM CATALOG BY ID
        // GET api/catalog/5
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CatalogItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCatalogItem(int id)
        {
            await Task.Delay(1000);
            var catalogItem = _dataStore.GetCatalogItemById(id);
            if (catalogItem == null)
            {
                return NotFound($"Item ID {id} not exist"); 
            }
            return Ok(catalogItem);


        }

        // SUB STOCK QTY BY ID AND QTY
        // GET api/catalog/substock/{id}/{qty}
        [HttpGet("substock/{id}/{qty}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SubStockQty(int id, int qty)
        {
            await Task.Delay(1000);
            if(qty<=0){
                return BadRequest($"Qty should be > 0 but you send: {qty}");
            }
            var catalogItem = _dataStore.GetCatalogItemById(id);
            if (catalogItem == null)
            {
                return NotFound($"Item ID {id} not exist");
            }
            if (qty > catalogItem.StockQty){
                return BadRequest($"Qty should be <= {catalogItem.StockQty} but you send: {qty}");
            }
            catalogItem.StockQty -= qty;
            return Ok($"{catalogItem.Name}'s Qty updated, new stock available is {catalogItem.StockQty}");
        }

    }
}
