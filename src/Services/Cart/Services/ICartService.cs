﻿using System;
using System.Threading.Tasks;
using Cart.Models;

namespace Cart.Services
{
    public interface ICartService
    {
        Task<bool> CheckOut(CartModel cart);
        Task<bool> StockAvailableCart(CartModel cart);
        Task<CatalogItem> GetCatalogItem(int cartItemId);
    }
}