﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Cart.Models;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.IO;

namespace Cart.Services
{
    public class CartService : ICartService
    {
        private HttpClient _httpClient;
        private string _catalogEndpoint;
        private string _orderEndpoint;

        public CartService(HttpClient httpClient){
            _catalogEndpoint = Environment.GetEnvironmentVariable("CATALOG_ENDPOINT");
            _orderEndpoint = Environment.GetEnvironmentVariable("ORDER_ENDPOINT");
            _httpClient = httpClient;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "ShoppingAPI Cart Service");
        }

        public async Task<bool> CheckOut(CartModel cart){
            var order = new OrderModel { UserId= cart.UserId, OrderItems = cart.CartItems, Total = cart.Total};
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(OrderModel));

            // use the serializer to write the object to a MemoryStream 
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, order);
            ms.Position = 0;

            //use a Stream reader to construct the StringContent (Json) 
            StreamReader sr = new StreamReader(ms);
            StringContent theContent = new StringContent(sr.ReadToEnd(), System.Text.Encoding.UTF8, "application/json");

            //Post the data 
            var aResponse = await _httpClient.PostAsync(_orderEndpoint, theContent);

            if (!aResponse.IsSuccessStatusCode)
            {
                String message = "HTTP Status: " + aResponse.StatusCode.ToString() + " - Reason: " + aResponse.ReasonPhrase;
                Console.Write(message);
                return false;
            }
            order.OrderItems.ForEach(i => {
                _httpClient.GetStringAsync($"{_catalogEndpoint}/substock/{i.Id}/{i.Qty}");
            });
            return true;
        }

        public async Task<bool> StockAvailableCart(CartModel cart){
            var itemTaskList = cart.CartItems.Select(item => new { Item = item, catalogItem = GetCatalogItem(item.Id) }).ToList();
            await Task.WhenAll(itemTaskList.Select(x => x.catalogItem));
            return itemTaskList.All(x => x.catalogItem.Result.StockQty >= x.Item.Qty);
        }


        public async Task<CatalogItem> GetCatalogItem(int cartItemId)
        {
            var serializer = new DataContractJsonSerializer(typeof(CatalogItem));
            var path = $"{_catalogEndpoint}/{cartItemId}";
            try
            {
                var streamTask = _httpClient.GetStreamAsync(path);
                var catalogItem = serializer.ReadObject(await streamTask) as CatalogItem;
                return catalogItem;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return null;
            }

        } 
    }
}
