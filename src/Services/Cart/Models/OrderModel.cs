﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cart.Models
{
    [DataContract]
    public class OrderModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "userID")]
        public int UserId { get; set; }

        [DataMember(Name = "orderItems")]
        public List<CartItem> OrderItems { get; set; }

        [DataMember(Name = "total")]
        public decimal Total { get; set; }

        public OrderModel() { }
    }
}
