﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cart.Models
{
    [DataContract]
    public class CatalogItem
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        [DataMember(Name = "stockQty")]
        public int StockQty { get; set; }
    }
}