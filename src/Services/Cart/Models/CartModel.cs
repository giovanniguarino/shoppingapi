﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cart.Models
{
    public class CartModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public List<CartItem> CartItems { get; set; }

        public decimal Total { get; set; }

        public CartModel() { }
    }
}