﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cart.DataStore;
using Cart.Models;
using System.Net;
using System.Net.Http;
using Cart.Services;

namespace Cart.Controllers
{
    [Route("api/[controller]")]
    public class CartController : Controller
    {
        protected ICartDataStore _dataStore;
        protected ICartService _cartService;

        public CartController(ICartDataStore dataStore, ICartService cartService){
            _dataStore = dataStore;
            _cartService = cartService; 
        }


        // CREATE A NEW USER'S CART
        // GET api/cart/create/{userId}
        [HttpGet("/create/{userId}")]
        [ProducesResponseType(typeof(CartModel),(int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateCart(int userId)
        {
            await Task.Delay(1000);
            var cart = _dataStore.CreateCart(userId);
            return Ok(cart);
        }

        // GET A CART BY CARTID
        // GET api/cart/{cartId}
        [HttpGet("{cartId}")]
        [ProducesResponseType(typeof(CartModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCart(int cartId)
        {
            await Task.Delay(1000);
            var cart = _dataStore.GetCartById(cartId);
            if (cart == null)
            {
                return NotFound($"Cart {cartId} not found!");
            }
            return Ok(cart);
        }

        // ADD AN ITEM TO CART
        // GET api/cart/{cartId}/add/{cartItemId}/{qty}
        [HttpGet("{cartId}/add/{cartItemId}/{qty}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(CartModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddCartItem(int cartId, int cartItemId, int qty)
        {
            var cart = _dataStore.GetCartById(cartId);
            if (cart == null)
            {
                return NotFound($"Cart {cartId} not found!");
            }

            var catalogItem = await _cartService.GetCatalogItem(cartItemId);

            if (catalogItem == null)
            {
                return NotFound($"Item {cartItemId} not found!");
            }

            if (catalogItem.StockQty < qty)
            {
                return BadRequest("Stock qty not available!");
            }

            cart.CartItems.Add(new CartItem { Id = catalogItem.Id, Name = catalogItem.Name, Price = catalogItem.Price, Qty = qty });

            cart.CartItems = cart.CartItems.GroupBy(c => c.Id).Select(cl => new CartItem
            {
                Id = cl.First().Id,
                Name = cl.First().Name,
                Price = cl.First().Price,
                Qty = cl.Sum(c => c.Qty),
            }).ToList();

            cart.Total = cart.CartItems.Sum(i => i.Price * i.Qty);

            return Ok(cart);
        }

        // REMOVE AN ITEM FROM CART
        // GET api/cart/{cartId}/delete/{cartItemId}
        [HttpGet("{cartId}/delete/{cartItemId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CartModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteCartItem(int cartId, int cartItemId)
        {
            await Task.Delay(1000);
            var cart = _dataStore.GetCartById(cartId);
            if (cart == null)
            {
                return NotFound($"Cart {cartId} not found!");
            }
            cart.CartItems.RemoveAll(i => i.Id == cartItemId);
            cart.Total = cart.CartItems.Sum(i => i.Price * i.Qty);
            return Ok(cart);
        }


        // COMPLETE CHECKOUT AND CREATE A NEW ORDER
        // GET api/cart/{cartId}/checkout
        [HttpGet("{cartId}/checkout")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(OrderModel),(int)HttpStatusCode.OK)]
        public async Task<IActionResult> Checkout(int cartId)
        {
            var cart = _dataStore.GetCartById(cartId);
            if(cart == null){
                return NotFound($"Cart {cartId} not found!");
            }
            var stockAvailable = await _cartService.StockAvailableCart(cart);

            if(!stockAvailable){
                return BadRequest("Stock Qty not available!");
            }

            if(!await _cartService.CheckOut(cart)){
                return BadRequest("Checkout error!");
            }
            cart.CartItems.RemoveAll(i => true);
            cart.Total = 0;
            return Ok("Checkout completed!");
        }






    }
}
