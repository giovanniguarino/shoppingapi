﻿using System;
using System.Collections.Generic;
using Cart.Models;
using System.Linq;

namespace Cart.DataStore
{
    public class CartDataStore : ICartDataStore
    {
        private IDictionary<int, CartModel> _cartList;

        public CartDataStore() => _cartList = new Dictionary<int, CartModel>();

        public CartModel CreateCart(int userId)
        {
            var id = _cartList.Count() + 1;
            var cart = new CartModel { Id = id, UserId = userId, CartItems= new List<CartItem>(), Total = 0};
            _cartList.Add(cart.Id, cart);
            return cart;
        }

        public IEnumerable<CartModel> GetAllCart() => _cartList.Values.ToList();

        public CartModel GetCartById(int cartId)
        {
            if(!_cartList.ContainsKey(cartId))
            {
                return null;
            }

            return _cartList[cartId];
        }


        public bool UpdateCart(CartModel cart)
        {
            if (!_cartList.ContainsKey(cart.Id))
            {
                return false;
            }

            var total = cart.CartItems.Sum(c => c.Price * c.Qty);
            cart.Total = total;
            _cartList[cart.Id] = cart;
            return true;
        }


    }
}
