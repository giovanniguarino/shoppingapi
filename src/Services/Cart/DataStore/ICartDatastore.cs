﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Cart.Models;


namespace Cart.DataStore
{
    public interface ICartDataStore
    {
        CartModel CreateCart(int userId);
        IEnumerable<CartModel> GetAllCart();
        CartModel GetCartById(int cartId);
        bool UpdateCart(CartModel cart);
    }
}