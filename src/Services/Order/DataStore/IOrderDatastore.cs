﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Order.Models;


namespace Order.DataStore
{
    public interface IOrderDataStore
    {
        void CreateOrder(OrderModel order);
        IEnumerable<OrderModel> GetAllOrdersByUserId(int userId);
    }
}