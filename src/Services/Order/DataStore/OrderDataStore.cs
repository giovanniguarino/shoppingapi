﻿using System;
using System.Collections.Generic;
using Order.Models;
using System.Linq;

namespace Order.DataStore
{
    public class OrderDataStore : IOrderDataStore
    {
        private IDictionary<int, OrderModel> _orderList;

        public OrderDataStore() => _orderList = new Dictionary<int, OrderModel>();

        public void CreateOrder(OrderModel order)
        {
            var id = _orderList.Count() + 1;
            order.Id = id;
            _orderList.Add(order.Id, order);
        }

        public IEnumerable<OrderModel> GetAllOrdersByUserId(int userId){
            return _orderList.Values.Where(o => o.UserId == userId).ToList();
        }


    }
}
