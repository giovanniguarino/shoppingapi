﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Order.Models;
using Order.DataStore;
using System.Net;

namespace Order.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {

        protected IOrderDataStore _dataStore;
        public OrderController(IOrderDataStore dataStore) => _dataStore = dataStore;

        // GET ALL ORDER BY USERID
        // GET api/order/all/userId
        [HttpGet("all/{userId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<OrderModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllOrdersByUserId(int userId)
        {
            await Task.Delay(1000);
            var orders = _dataStore.GetAllOrdersByUserId(userId);
            if(orders.Count()==0){
                return NotFound($"Orders for userId {userId} not found!");
            }
            return Ok(orders);
        }

        // CREATE NEW ORDER
        // POST api/order
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(OrderModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody]OrderModel order)
        {
            await Task.Delay(1000);
            if(order.OrderItems == null){
                return BadRequest("OrderItems can't be null!");
            }

            if (order.OrderItems.Count == 0)
            {
                return BadRequest("OrderItems can't be empty!");
            }
            _dataStore.CreateOrder(order);
            return Ok("Order created!");
        }

    }
}
