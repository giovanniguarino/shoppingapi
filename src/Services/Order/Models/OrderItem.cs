﻿using System;
using System.Collections.Generic;

namespace Order.Models
{
    public class OrderItem
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Qty { get; set; }

        public OrderItem() { }
    }
}