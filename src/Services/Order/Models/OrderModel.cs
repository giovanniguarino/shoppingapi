﻿using System;
using System.Collections.Generic;

namespace Order.Models
{
    public class OrderModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public List<OrderItem> OrderItems { get; set; }

        public decimal Total { get; set; }

        public OrderModel() { }
    }
}
